import java.util.ArrayList;

public class Phonebook{
    private ArrayList<Contact> contacts;

    // constructors
    public Phonebook(){
        contacts = new ArrayList<>();
    }

    public Phonebook(ArrayList<Contact> contact){
        contacts = contact;
    }

    // setters
    public void setContacts(ArrayList<Contact> contacts){
        this.contacts = contacts;
    }

    public void addContact(Contact contact){
        contacts.add(contact);
    }

    // getters
    public ArrayList<Contact> getContacts(){
        return contacts;
    }

    public void printContactInfo(){
        if(contacts.isEmpty()){
            System.out.println("Phonebook is empty.");
        }
        else{
            for(Contact contact: contacts){
                System.out.println(contact.getName());
                System.out.println("-----------------------------");
                System.out.println(contact.getName() + " has the following registered number:");
                System.out.println(contact.getContactNumber());
                System.out.println(contact.getName() + " has the following registered address:");
                System.out.println(contact.getAddress());
            }
        }
    }

}